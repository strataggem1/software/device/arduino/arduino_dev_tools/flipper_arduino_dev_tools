 /*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef _VARIANT_FEATHER52840_
#define _VARIANT_FEATHER52840_

/** Master clock frequency */
#define VARIANT_MCK       (64000000ul)

#define USE_LFXO      // Board uses 32khz crystal for LF
// define USE_LFRC    // Board uses RC for LF

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "WVariant.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

// Number of pins defined in PinDescription array
#define PINS_COUNT           (44)
#define NUM_DIGITAL_PINS     (44)
#define NUM_ANALOG_INPUTS    (1)
#define NUM_ANALOG_OUTPUTS   (0)

// LEDs
#define PIN_USER_LED1        (22)
#define PIN_USER_LED2        (20)
#define PIN_USER_LED3        (18)


#define LED_RED              PIN_USER_LED1
#define LED_GREEN            PIN_USER_LED2
#define LED_BLUE             PIN_USER_LED3

#define LED_STATE_ON         1         // State when LED is litted

/*
 * Buttons
 */
#define USR_BTN             (23)

/*
 * Analog pins
 */
#define PIN_A0               (41)


static const uint8_t A0  = PIN_A0 ;

#define ADC_RESOLUTION    14

// Other pins
#define PIN_VBAT_MES       A0

/*
 * Serial interfaces
 */
#define PIN_SERIAL1_RX       (9)
#define PIN_SERIAL1_TX       (8)

#define PIN_SERIAL2_RX       (7)
#define PIN_SERIAL2_TX       (6)


/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 2

#define PIN_SPI_MISO         (2)
#define PIN_SPI_MOSI         (3)
#define PIN_SPI_SCK          (5)


static const uint8_t SS  = (4);
static const uint8_t MOSI = PIN_SPI_MOSI ;
static const uint8_t MISO = PIN_SPI_MISO ;
static const uint8_t SCK  = PIN_SPI_SCK ;



#define PIN_SPI1_MISO         (35)
#define PIN_SPI1_MOSI         (34)
#define PIN_SPI1_SCK          (32)


static const uint8_t SS1  = (33);
static const uint8_t MOSI1= PIN_SPI1_MOSI ;
static const uint8_t MISO1 = PIN_SPI1_MISO ;
static const uint8_t SCK1  = PIN_SPI1_SCK ;
#define SDCARD_EN_PIN  (36)
#define SDCARD_DETECT_PIN  (37)


/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 2

#define PIN_WIRE_SDA         (0)
#define PIN_WIRE_SCL         (1)

#define PIN_WIRE1_SDA         (11)
#define PIN_WIRE1_SCL         (13)

// QSPI Pins
#define PIN_QSPI_SCK         (26)
#define PIN_QSPI_CS          (27)
#define PIN_QSPI_IO0         (28)
#define PIN_QSPI_IO1         (29)
#define PIN_QSPI_IO2         (30)
#define PIN_QSPI_IO3         (31)

// On-board QSPI Flash
#define EXTERNAL_FLASH_DEVICES   MX25R3235FM2IL0
#define EXTERNAL_FLASH_USE_QSPI

//Expander not affected pins
#define EXP_25               (10)
#define EXP_27               (12)
#define EXP_29               (14)
#define EXP_30               (15)
#define EXP_31               (16)
#define EXP_32               (17)
#define EXP_34               (19)
#define EXP_36               (21)
#define EXP_39               (24)
#define EXP_40               (25)

//XLB pins
#define XLB_EN               (38)
#define XLB_RST              (39)
#define XLB_SPI_CS           (40)


//Output voltage enabler

#define SW_3V3_EN           (42)
#define SW_VDD_EN           (43)

#ifdef __cplusplus
}
#endif

/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#endif
