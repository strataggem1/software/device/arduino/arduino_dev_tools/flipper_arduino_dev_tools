/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.
  Copyright (c) 2016 Sandeep Mistry All right reserved.
  Copyright (c) 2018, Adafruit Industries (adafruit.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "variant.h"
#include "wiring_constants.h"
#include "wiring_digital.h"
#include "nrf.h"

const uint32_t g_ADigitalPinMap[] =
{
  //EXP calculation methode IDENTIFIER = PORT*32 + PIN_NUM
  // expander J402
  11,  // EXP7 is P0.11 (I2C0_SDA)
  8,   // EXP9 is P0.08 (I2C0_SCL)
  40,  // EXP10 is P1.08 (SPI0_MISO)
  6,   // EXP11 is P0.06 (SPI0_MOSI)
  25,  // EXP12 is P0.25 (SPI0_/CS1)
  4,   // EXP13 is P0.04 (SPI0_SCLK)
  14,  // EXP14 is P0.14 (UART1_TX)
  16,  // EXP15 is P0.16 (UART1_RX)
  13,  // EXP16 is P0.13 (UART0_TX)
  15,  // EXP17 is P0.15 (UART0_RX)


  //expander J401
  33,  // EXP25  is P1.01
  12,  // EXP26 is P0.12 (I2C1_SDA)
  43,  // EXP27  is P1.11
  41,  // EXP28 is P1.09 (I2C1_SCL)
  30,  // EXP29  is P0.30 (AIN6)
  45,  // EXP30 is P1.13
  2,   // EXP31 is P0.02 (AIN0)
  28,  // EXP32 is P0.28 (AIN4)
  42,  // EXP33 is P1.10 (LED_BLUE)
  29,  // EXP34 is P0.29 (AIN5)
  39,  // EXP35 is P1.07 (LED_GREEN)
  3,   // EXP36 is P0.03 (EN_EXP)
  37,  // EXP37 is P1.05 (LED_RED)
  38,  // EXP38 is P1.06 (USER_BTN)
  9,   // EXP39 is P0.09 (NFC1)
  10,  // EXP40 is P0.10 (NFC2)


  // on board QSPI FLASH MX25R3235FM2IL0
  19,  // FLASH_CLK is P0.19 (QSPI CLK)
  17,  // FLASH_/CS is P0.17 (QSPI CS)
  20,  // FLASH_IO0/FLASH_MOSI is P0.20 (QSPI Data 0)
  21,  // FLASH_IO1/FLASH_MISO is P0.21 (QSPI Data 1)
  22,  // FLASH_IO2 is P0.22 (QSPI Data 2)
  23,  // FLASH_IO3 is P0.23 (QSPI Data 3)
 
 // on board SD card reader
  7,   // SD_SCLK is P0.07 (SD_SPI_SCLK)
  5,   // SD_/CS is P0.05 (SD_SPI_/CS)
  27,  // SD_MOSI is P0.27 (SD_SPI_MOSI)
  26,  // SD_SCLK is P0.26 (SD_SPI_MISO)
  47,  // SDCARD_EN is P1.15 (SD_EN_PIN)
  46,  // SD_DETECT is P1.14 (SD_DETECT_PIN)
  
  // Xlbee pins
  35, // MCU_XLB_EN is P.1.3
  34, // !XLB_RST!  is P.1.2
  24, // XLBEE37 is P.024 (SPI0_CS0)


  // Analog pins
  31, //VBAT_MES is P0.31 (AIN7)

  // Output Voltage enable pins
  44, //3V3_SW_EN is P1.12
  36, //VDD_SW_EN is P1.04


  // Thus, there are 39 defined pins

  // The remaining pins are not usable:
  //
  // The following pins were never listed as they were considered unusable
  //  0,      // P0.00 is XL1   (attached to 32.768kHz crystal)
  //  1,      // P0.01 is XL2   (attached to 32.768kHz crystal)
  // 18,      // P0.18 is RESET (attached to switch)
  // 32,      // P1.00 is SWO   (attached to debug header)
  
};

void initVariant()
{
  // LED1 & LED2
  pinMode(PIN_USER_LED1, OUTPUT);
  digitalWrite(PIN_USER_LED1, LOW);

  pinMode(PIN_USER_LED2, OUTPUT);
  digitalWrite(PIN_USER_LED2, LOW);

  pinMode(PIN_USER_LED3, OUTPUT);
  digitalWrite(PIN_USER_LED3, LOW);


}

